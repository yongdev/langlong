import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import SignUp from '../views/SignUp.vue'

import itemDetail from '../views/itemDetail.vue'
import Cart from '../views/Cart.vue'
import GolfClub from '../views/GolfClub.vue'
import Progress from '../views/Progress.vue'
// import CartNormal from '../views/cart/CartNormal.vue'
// import CartPick from '../views/cart/CartPick.vue'
// import CartQuick from '../views/cart/CartQuick.vue'

import Main from '../views/igms/Main.vue'





import UserCreate from '../views/firebase/UserCreate.vue'
import UserList from '../views/firebase/UserList.vue'




Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },  
  {
    path: '/signup',
    name: 'SignUp',
    component: SignUp
  },  
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },   
  {
    path: '/golfclub',
    name: 'GolfClub',
    component: GolfClub
  },
  {
    path: '/add',
    name: 'UserCreate',
    component: UserCreate
  },  
  {
    path: '/list',
    name: 'UserList',
    component: UserList
  },  
  {
    path: '/detail/:id',
    component: itemDetail
  },  
  {
    path: '/cart',
    component: Cart,
   },  
   {
    path: '/gms',
    name: 'Main',
    component: Main
  },   
  {
    path: '/mypage',
    name: 'Progress',
    component: Progress,
    meta : {
      reload:true
    }
  },    
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})




export default router
