import Vue from 'vue'
import App from './App.vue'
import router from './router'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import VueGeolocationApi from 'vue-geolocation-api'
import firebase from 'firebase'
import { firestorePlugin } from 'vuefire'
import store from './store'
import VueCookies from "vue-cookies"
import VueTimepicker from 'vue2-timepicker'
// import VueCookie from "vue-cookie"


import VCalendar from 'v-calendar';

const firebaseConfig = {
  apiKey: "AIzaSyDIxycFGlu7Nn02eNTp6FtXEILwIKwyzRA",
  authDomain: "vue-barogo.firebaseapp.com",
  databaseURL: "https://vue-barogo.firebaseio.com",
  projectId: "vue-barogo",
  storageBucket: "vue-barogo.appspot.com",
  messagingSenderId: "122726898776",
  appId: "1:122726898776:web:9e2d9294cfa88f7e84fe2d",
  measurementId: "G-XQFWQPGVX2"
};
// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig)

// let app = Firebase.initializeApp(config)
// export const db = app.database ()
Vue.use(VueCookies)
Vue.use(VCalendar)
Vue.use(VueTimepicker)
// Vue.user(VueCookie);

Vue.use(VueGeolocationApi/*, { ...options } */)
// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

Vue.use(firestorePlugin)
Vue.config.productionTip = false
const db = firebaseApp.firestore();

Vue.prototype.$fireDB = db
new Vue({
  router,
  store,

  render: h => h(App)
}).$mount('#app')


export { db }
