import Vue from 'vue'
import Vuex from 'vuex'
import firebase from 'firebase'
// import createPersistedState from 'vuex-persistedstate';
// import { db } from '../main';

Vue.use(Vuex)

export default new Vuex.Store({
  
  state: {
    Items : [],
    Option : {},
    email:'',
    password:'',
    user:'',
    isLogin:false,
    markets : [
      [
        1,
        '잠실점',
        37.506665,
         127.088535,
        '서울특별시 송파구 잠실본동 251-3',
        0
    ],
      [
        2,
        '방이점',
        37.513463, 
         127.113877,
        '서울특별시 송파구 방이2동 백제고분로 478',
        0
    ],
    [
        3,
        '둔촌점',
        37.531648, 
         127.137264,
        '성내동 396-16번지 강동구 서울특별시 KR',
        0
    ],
      [
        4,
        '송파점',
        37.493488, 
         127.117999,
        '서울특별시 송파구 문정2동 150-2',
        0
    ],
    [
        5,
        '광장점',
        37.545374, 
         127.091580,
        '구의동 228-2번지 1층 씨엔씨빌딩 광진구 서울특별시 KR',
        0
    ],
      [
        6,
        '압구정점',
        37.520196, 
         127.027363,
        '서울특별시 강남구 논현동 7-13',
        0
    ],
      [
        7,
        '도곡점',
        37.494920, 
         127.043943,
        '역삼동 787-9번지 강남구 서울특별시 KR',
        0
    ],
      [
        8,
        '양재점',
        37.482908, 
         127.037476,
        '서울특별시 서초구 양재1동 강남대로 190',
        0
    ],    
      [
        9,
        '광장점',
        37.545374, 
         127.091580,
        '구의동 228-2번지 1층 씨엔씨빌딩 광진구 서울특별시 KR',
        0
    ],      
      [
        10,
        '서초점',
        37.484235, 
         127.019882,
        '서울특별시 서초구 남부순환로 2465 더 골프에비뉴 혜준빌딩 1층',
        0
    ],     
      [
        11,
        '반포점',
        37.500303, 
         126.995728,
        '서울특별시 서초구 반포동 사평대로 98 진영빌딩',
        0
    ],      
      [
        12,
        '당산점',
        37.536379, 
         126.896388,
        '서울특별시 영등포구 양평동4가 선유로 233 1층',
        0
    ],      
      [
        13,
        '약수점',
        37.559125, 
         127.009287,
        '서울특별시 중구 신당2동 동호로 214',
        0
    ],      
      [
        14,
        '분당금곡점',
        37.353703, 
         127.102470,
        '경기도 성남시 분당구 금곡동 대왕판교로 110',
        0
    ],      
      [
        15,
        '평촌점',
        37.395559, 
         126.975197,
        '경기도 안양시 동안구 평촌동 96-2번지 KR',
        0
    ],   
      [
        16,
        '부천점',
        37.516352, 
         126.775203,
        '경기 부천시 원미구 중동 1035-2번지',
        0
    ],  
      [
        17,
        '김포현대아울렛점',
        37.599709, 
         126.786136,
        '경기도 김포시 고촌읍 아라육로152번길 100',
        0
    ], 
      [
        18,
        '대치점',
        37.502111, 
         127.066407,
        '서울 강남구 영동대로 311',
        0
      ], 
      [
        19,
        '마곡점',
        37.566825, 
         126.843768,
        '서울 강서구 가양동 99-2',
        0
     ], 
      [
        20,
        '수원인계점',
        37.269438, 
         127.026928,
        '경기 수원시 팔달구 인계동 966-9',
        0
      ],                       


    ]

  },
  mutations: {
    setUser(payload) {
      console.log(payload, this.state)
      this.state.isLogin = true
      // let token = this.state.user
      
      // localStorage.setItem('token',token)
      firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION)
      .then(function(res) {
        console.log(res)
        
      })
      .catch(function(error) {
        // Handle Errors here.
        console.log(error)
      });
    },
    logout() {
      
      this.state.isLogin = false
      firebase.auth().signOut().then(function() {
        alert('로그아웃 되었습니다')
      }).catch(function(error) {
        console.log(error)
      });
    }
  },
  actions: {
    login({commit},payload) {
      firebase.auth().signInWithEmailAndPassword(payload.email, payload.password)
          .then(
              (user) => {
                  alert('로그인 완료')
                  console.log(user.user.uid)
                  this.state.email = payload.email
                  this.state.password = payload.password
                  this.state.user = user.user.uid
                  commit('setUser',payload)
              },
              (err) => {
                  alert('에러 : ' + err.message)
              }
          );
      },
      // GetCart() {
      //   db.collection('order').onSnapshot((snapshotChange) => {
            
      //     snapshotChange.forEach((doc) => {                
      //         let sub = {
      //             uuid : doc.id,
      //             id: doc.data().id,
      //             start: doc.data().start,
      //             arrive: doc.data().arrive,
      //             date: doc.data().date,
      //             time: doc.data().time,
      //             totalPrice: doc.data().totalPrice,
                  
      //         }
              
             
      //         for(let i=0; i<doc.data().count.length; i++) {
      //             let subList = [ doc.data().count[i], doc.data().name[i] , doc.data().price[i]]
      //             this.state.Option[i] = subList
      //         }
      //         sub['option'] = this.Option
      //         this.state.Items.push(sub)
              


      //     });
      // })
      // console.log(this.Items)        
      // }

  },
  getters: {
    getMarketInfo : state => {
      return state.markets
    }
  }
})
